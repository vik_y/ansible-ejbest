If you are managing system accounts with ansible then ideally you should not use passwords. 
Make users login using their ssh public key. 


# Ansible installation

This setup requires ansible version 2.7.1 + 

To upgrade: Remove ansible if installed using brew or apt. Install ansible using pip 

`sudo pip install --upgrade ansible`


# Post Installation Steps

Before running this playbook you must ensure that all the required roles are fetched from ansible-galaxy 

```
ansible-galaxy install geerlingguy.java
ansible-galaxy install geerlingguy.ntp
ansible-galaxy install geerlingguy.docker
ansible-galaxy install geerlingguy.firewall
```

# Java 

Please be informed that this will installed the latest java runtime. Right now 1.10 

# Changing Password

Use script `generate-pass.sh` to generate password hash. Copy paste the hash to the `password`
of user who's password you want to change. Run ansible. 
