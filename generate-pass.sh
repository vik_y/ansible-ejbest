#!/bin/bash

echo "Please enter cleartext password:"
read cleartext 

echo "You have entered password:"
echo $cleartext


hash=$(echo $cleartext |  mkpasswd --method=sha-256 -s)
echo "Use the below hashed password with ansible:"
echo $hash